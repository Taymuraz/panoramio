﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using Buffer = Windows.Storage.Streams.Buffer;

namespace Panoramio
{
    public static class Helpers
    {
        public static async Task<string> GetResponseStringAsync(string url)
        {
            var request = WebRequest.Create(url);

            try
            {
                using (var response = await request.GetResponseAsync())
                using (var responseStream = response.GetResponseStream())
                {
                    string data;
                    using (var reader = new StreamReader(responseStream))
                    {
                        data = await reader.ReadToEndAsync();
                    }

                    return data;
                }
            }
            catch (Exception e)
            {
                e.Log();
            }

            return null;
        }

        public static async Task CopyStreamToFileAsync(IRandomAccessStream stream, StorageFile file)
        {
            using (var fileStream = await file.OpenAsync(FileAccessMode.ReadWrite))
            {
                var bufferSize = (uint)stream.Size;
                var buffer = new Buffer(bufferSize);
                IBuffer readBuffer;

                while ((readBuffer = await stream.ReadAsync(buffer, bufferSize, InputStreamOptions.Partial)).Length > 0)
                    await fileStream.WriteAsync(readBuffer);
            }
        }
    }
}