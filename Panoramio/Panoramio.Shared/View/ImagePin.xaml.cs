﻿using System;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Shapes;

namespace Panoramio.View
{
    public sealed partial class ImagePin : ImageWithProgressBase
    {
        public ImagePin()
        {
            InitializeComponent();

            LayoutRoot.DataContext = this;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            UpdateIndicator(BackgroundEllipse);
            UpdateIndicator(ProgressEllipse);

            return base.ArrangeOverride(finalSize);
        }

        private void UpdateIndicator(Ellipse ellipse)
        {
            var strokeThickness = ellipse.ActualWidth/2;
            var transform = (CompositeTransform)ellipse.RenderTransform;
            transform.CenterX = strokeThickness;
            transform.CenterY = strokeThickness;
            transform.TranslateY = strokeThickness;
            ellipse.StrokeThickness = strokeThickness;
        }

        protected override void OnImageOpened(object sender, RoutedEventArgs routedEventArgs)
        {
            base.OnImageOpened(sender, routedEventArgs);

            ZoomInImageStoryboard.Begin();
        }

        protected override void OnImageDownloadProgress(object sender, DownloadProgressEventArgs downloadProgressEventArgs)
        {
            var strokeDashLength = (ProgressEllipse.ActualWidth - ProgressEllipse.StrokeThickness) * Math.PI / ProgressEllipse.StrokeThickness * downloadProgressEventArgs.Progress / 100;
            var strokeDashOffset = (ProgressEllipse.ActualWidth - ProgressEllipse.StrokeThickness) * Math.PI / ProgressEllipse.StrokeThickness;
            ProgressEllipse.StrokeDashArray = new DoubleCollection { strokeDashLength, strokeDashOffset };
        }
    }
}