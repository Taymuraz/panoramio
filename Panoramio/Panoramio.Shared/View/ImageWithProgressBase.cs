﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace Panoramio.View
{
    public class ImageWithProgressBase : UserControl, INotifyPropertyChanged
    {
        private bool mImageLoaded;
        private int mProgress;
        public event PropertyChangedEventHandler PropertyChanged;

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(BitmapImage), typeof(ImageWithProgressBase), new PropertyMetadata(default(BitmapImage), OnImageChanged));

        public BitmapImage Source
        {
            get { return (BitmapImage)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        public bool ImageLoaded
        {
            get { return mImageLoaded; }
            set
            {
                mImageLoaded = value;
                RaisePropertyChanged();
            }
        }

        public int Progress
        {
            get { return mProgress; }
            set
            {
                mProgress = value;
                RaisePropertyChanged();
            }
        }

        protected static void OnImageChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var imageWithProgressBase = (ImageWithProgressBase)dependencyObject;

            var oldImage = dependencyPropertyChangedEventArgs.OldValue as BitmapImage;
            if (oldImage != null)
            {
                oldImage.DownloadProgress -= imageWithProgressBase.OnImageDownloadProgress;
                oldImage.ImageOpened -= imageWithProgressBase.OnImageOpened;
            }

            var newImage = dependencyPropertyChangedEventArgs.NewValue as BitmapImage;
            if (newImage != null)
            {
                if (newImage.PixelWidth == 0 && newImage.PixelHeight == 0)
                {
                    imageWithProgressBase.ImageLoaded = false;
                    imageWithProgressBase.Progress = 0;
                }
                newImage.DownloadProgress += imageWithProgressBase.OnImageDownloadProgress;
                newImage.ImageOpened += imageWithProgressBase.OnImageOpened;
            }
        }

        protected virtual void OnImageOpened(object sender, RoutedEventArgs routedEventArgs)
        {
            ImageLoaded = true;
        }

        protected virtual void OnImageDownloadProgress(object sender, DownloadProgressEventArgs downloadProgressEventArgs)
        {
            Progress = downloadProgressEventArgs.Progress;
        }

        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
