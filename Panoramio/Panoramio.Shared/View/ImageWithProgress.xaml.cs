﻿using Windows.UI.Xaml.Media.Imaging;

namespace Panoramio.View
{
    public sealed partial class ImageWithProgress : ImageWithProgressBase
    {
        public ImageWithProgress()
        {
            InitializeComponent();

            LayoutRoot.DataContext = this;
        }
    }
}
