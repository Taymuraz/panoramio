﻿using System;
using System.Windows.Input;
using Windows.UI.Xaml;
using Microsoft.Xaml.Interactivity;

#if WINDOWS_APP
using Bing.Maps;
#elif WINDOWS_PHONE_APP
using Panoramio.Data;
#endif

namespace Panoramio.View
{
    class MapEventBehavior : DependencyObject, IBehavior
    {
        public DependencyObject AssociatedObject { get; set; }

        public static readonly DependencyProperty ChangeBoundsCommandProperty =
            DependencyProperty.Register("ChangeBoundsCommand", typeof(ICommand), typeof(MapEventBehavior), new PropertyMetadata(default(ICommand)));

        public ICommand ChangeBoundsCommand
        {
            get { return (ICommand)GetValue(ChangeBoundsCommandProperty); }
            set { SetValue(ChangeBoundsCommandProperty, value); }
        }

        public void Attach(DependencyObject associatedObject)
        {
            var mapView = associatedObject as MapView;

            if (mapView == null)
                throw new ArgumentException($"{nameof(MapEventBehavior)} can be attached only to MapView");

            AssociatedObject = associatedObject;

            mapView.BoundsChangeEnded += OnMapViewBoundsChangeEnded;
        }

        public void Detach()
        {
            var mapView = (MapView)AssociatedObject;
            mapView.BoundsChangeEnded -= OnMapViewBoundsChangeEnded;

            AssociatedObject = null;
        }

        private void OnMapViewBoundsChangeEnded(object sender, LocationRect locationRect)
        {
            if (ChangeBoundsCommand != null && ChangeBoundsCommand.CanExecute(locationRect))
                ChangeBoundsCommand.Execute(locationRect);
        }
    }
}