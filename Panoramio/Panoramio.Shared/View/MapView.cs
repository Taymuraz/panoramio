﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Panoramio.Data;

#if WINDOWS_APP
using Bing.Maps;
#elif WINDOWS_PHONE_APP
using System.Threading;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.UI.Xaml.Controls.Maps;
#endif

namespace Panoramio.View
{
    public class MapView : Grid
    {
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(MapView), new PropertyMetadata(default(IEnumerable), OnItemsSourcePropertyChanged));

        public static readonly DependencyProperty ItemTemplateProperty =
            DependencyProperty.Register("ItemTemplate", typeof(DataTemplate), typeof(MapView), new PropertyMetadata(default(DataTemplate)));

        public static readonly DependencyProperty SelectedItemProperty = 
            DependencyProperty.Register("SelectedItem", typeof (object), typeof (MapView), new PropertyMetadata(default(object)));

        public event EventHandler<LocationRect> BoundsChangeEnded;

#if WINDOWS_APP
        private Map mMap;
        private MapLayer mMapLayer;
#elif WINDOWS_PHONE_APP
        private MapControl mMap;
#endif

        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }

        public object SelectedItem
        {
            get { return GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public MapView()
        {
#if WINDOWS_APP
            mMap = new Map();

            mMapLayer = new MapLayer();
            mMap.Children.Add(mMapLayer);
#elif WINDOWS_PHONE_APP
            mMap = new MapControl();
#endif

            Children.Add(mMap);

            SubscribeToMap();
        }

        ~MapView()
        {
            UnsubscribeFromMap();
        }

        private static void OnItemsSourcePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var control = sender as MapView;
            control?.OnItemsSourceChanged((IEnumerable)e.OldValue, (IEnumerable)e.NewValue);
        }

        private void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            var oldValueINotifyCollectionChanged = oldValue as INotifyCollectionChanged;
            if (oldValueINotifyCollectionChanged != null)
                oldValueINotifyCollectionChanged.CollectionChanged -= OnItemsSourceCollectionChanged;

            var newValueINotifyCollectionChanged = newValue as INotifyCollectionChanged;
            if (newValueINotifyCollectionChanged != null)
                newValueINotifyCollectionChanged.CollectionChanged += OnItemsSourceCollectionChanged;
        }

        private void OnItemsSourceCollectionChanged(object sender, NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            switch (notifyCollectionChangedEventArgs.Action)
            {
                case NotifyCollectionChangedAction.Reset:
#if WINDOWS_APP
                    mMapLayer.Children.Clear();
#elif WINDOWS_PHONE_APP
                    mMap.Children.Clear();
#endif
                    break;
                case NotifyCollectionChangedAction.Add:
                case NotifyCollectionChangedAction.Move:
                case NotifyCollectionChangedAction.Remove:
                case NotifyCollectionChangedAction.Replace:
                    if (notifyCollectionChangedEventArgs.NewItems != null)
                    {
                        foreach (var newItem in notifyCollectionChangedEventArgs.NewItems)
                        {
                            var itemElement = (FrameworkElement) ItemTemplate.LoadContent();
                            itemElement.DataContext = newItem;

                            var itemWithGeoposition = newItem as IHaveBasicGeoposition;
                            if (itemWithGeoposition != null)
                            {
                                var geoposition = itemWithGeoposition.BasicGeoposition;
#if WINDOWS_APP
                                MapLayer.SetPosition(itemElement, geoposition.ToLocation());
#elif WINDOWS_PHONE_APP
                                MapControl.SetLocation(itemElement, new Geopoint(geoposition));
#endif
                            }

                            itemElement.SizeChanged += OnItemElementSizeChanged;
                            itemElement.Tapped += OnItemElementTapped;

#if WINDOWS_APP
                            mMapLayer.Children.Add(itemElement);
#elif WINDOWS_PHONE_APP
                            mMap.Children.Add(itemElement);
#endif
                        }
                    }
                    if (notifyCollectionChangedEventArgs.OldItems != null)
                        foreach (var oldItem in notifyCollectionChangedEventArgs.OldItems)
                        {
#if WINDOWS_APP
                            var itemToRemove = mMapLayer.Children.FirstOrDefault(c => ((FrameworkElement)c).DataContext == oldItem);
#elif WINDOWS_PHONE_APP
                            var itemToRemove = mMap.Children.FirstOrDefault(c => ((FrameworkElement)c).DataContext == oldItem);
#endif
                            if (itemToRemove != null)
                            {

#if WINDOWS_APP
                                mMapLayer.Children.Remove(itemToRemove);
#elif WINDOWS_PHONE_APP
                                mMap.Children.Remove(itemToRemove);
#endif
                                var itemElement = (FrameworkElement) itemToRemove;
                                itemElement.SizeChanged -= OnItemElementSizeChanged;
                                itemElement.Tapped -= OnItemElementTapped;
                            }
                        }
                    break;
            }
        }

        private void OnItemElementTapped(object sender, TappedRoutedEventArgs tappedRoutedEventArgs)
        {
            var frameworkElement = (FrameworkElement)sender;
            SelectedItem = frameworkElement.DataContext;
        }

        private void OnItemElementSizeChanged(object sender, SizeChangedEventArgs sizeChangedEventArgs)
        {
            var frameworkElement = (FrameworkElement) sender;
            var newSize = sizeChangedEventArgs.NewSize;
            var newThickness = new Thickness(-newSize.Width / 2, -newSize.Height, newSize.Width / 2, newSize.Height);
            frameworkElement.Margin = newThickness;
        }

        private void SubscribeToMap()
        {
#if WINDOWS_APP
            mMap.ViewChangeEnded += OnMapViewChangeEnded;
#elif WINDOWS_PHONE_APP
            mMap.CenterChanged += OnMapViewChanged;
            mMap.ZoomLevelChanged += OnMapViewChanged;
#endif
        }

        private void UnsubscribeFromMap()
        {
#if WINDOWS_APP
            mMap.ViewChangeEnded -= OnMapViewChangeEnded;
#elif WINDOWS_PHONE_APP
            mMap.CenterChanged -= OnMapViewChanged;
            mMap.ZoomLevelChanged -= OnMapViewChanged;
#endif
        }

#if WINDOWS_APP
        private void OnMapViewChangeEnded(object sender, ViewChangeEndedEventArgs viewChangeEndedEventArgs)
        {
            RaiseBoundsChangeEnded(mMap.Bounds);
        }
#elif WINDOWS_PHONE_APP
        // workaround for lack of ViewChangeEnded event
        private CancellationTokenSource mEventRaiseCancellation = new CancellationTokenSource();

        private async void OnMapViewChanged(MapControl sender, object args)
        {
            mEventRaiseCancellation.Cancel();
            mEventRaiseCancellation = new CancellationTokenSource();
            var rect = mMap.GetBoundingRectangle();

            var cancellationToken = mEventRaiseCancellation.Token;
            await Task.Delay(100);
            if (cancellationToken.IsCancellationRequested)
                return;

            RaiseBoundsChangeEnded(rect);
        }
#endif

        private void RaiseBoundsChangeEnded(LocationRect rect)
        {
            BoundsChangeEnded?.Invoke(this, rect);
        }
    }
}