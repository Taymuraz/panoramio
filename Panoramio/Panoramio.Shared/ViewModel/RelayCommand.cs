﻿using System;
using System.Windows.Input;

namespace Panoramio.ViewModel
{
    public class RelayCommand : ICommand
    {
        private readonly Action mExecute;
        private readonly Func<bool> mCanExecute;

        public event EventHandler CanExecuteChanged;

        public RelayCommand(Action execute, Func<bool> canExecute = null)
        {
            mExecute = execute;
            mCanExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return mCanExecute == null || mCanExecute.Invoke();
        }

        public void Execute(object parameter)
        {
            mExecute.Invoke();
        }
    }

    public class RelayCommand<T> : ICommand
    {
        private readonly Action<T> mExecute;
        private readonly Func<T, bool> mCanExecute;

        public event EventHandler CanExecuteChanged;

        public RelayCommand(Action<T> execute, Func<T, bool> canExecute = null)
        {
            mExecute = execute;
            mCanExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return mCanExecute == null || mCanExecute.Invoke((T)parameter);
        }

        public void Execute(object parameter)
        {
            mExecute.Invoke((T)parameter);
        }
    }
}