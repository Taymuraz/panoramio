﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.ApplicationModel.DataTransfer;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Panoramio.Data;
#if WINDOWS_APP
using Bing.Maps;
#elif WINDOWS_PHONE_APP
using Windows.Phone.UI.Input;
using Windows.UI.ViewManagement;
#endif

namespace Panoramio.ViewModel
{
    public class MainPageViewModel : ViewModelBase
    {
        private DataTransferManager mDataTransferManager;
        private IRandomAccessStream mSharedMedia;

        private ObservableCollection<PanoramioPhoto> mPhotos = new ObservableCollection<PanoramioPhoto>();
        private PanoramioPhoto mSelectedPhoto;
        private int mBusyCounter;

        public bool PhoneMode =>
#if WINDOWS_APP
                false;
#elif WINDOWS_PHONE_APP
                true;
#endif

        public bool ShowBusyIndicator => !PhoneMode && BusyCounter > 0;

        public int BusyCounter
        {
            get { return mBusyCounter; }
            set
            {
                mBusyCounter = value;
#if WINDOWS_PHONE_APP
                var statusBar = StatusBar.GetForCurrentView();
                if (mBusyCounter > 0)
                {
                    statusBar.ProgressIndicator.Text = "";
                    statusBar.ProgressIndicator.ShowAsync();
                }
                else
                {
                    statusBar.ProgressIndicator.HideAsync();
                }
#endif
                RaisePropertyChanged();
                RaisePropertyChanged(nameof(ShowBusyIndicator));
            }
        }

        public ObservableCollection<PanoramioPhoto> Photos
        {
            get { return mPhotos; }
            set
            {
                mPhotos = value;
                RaisePropertyChanged();
            }
        }

        public PanoramioPhoto SelectedPhoto
        {
            get { return mSelectedPhoto; }
            set
            {
                mSelectedPhoto = value;
                RaisePropertyChanged();
            }
        }

        public ICommand ChangeBoundsCommand => new RelayCommand<LocationRect>(UpdatePhotosAsync);

        public ICommand DeselectCommand => new RelayCommand(() => SelectedPhoto = null);

        public ICommand SavePhoto => new RelayCommand(async () =>
        {
            BusyCounter++;
            try
            {
                using (var photoStream = await SelectedPhoto.GetHighQualityPhotoStreamAsync())
                {
                    var file = await KnownFolders.PicturesLibrary.CreateFileAsync(SelectedPhoto.TitleWithExtension, CreationCollisionOption.GenerateUniqueName);
                    var saveTask = Helpers.CopyStreamToFileAsync(photoStream, file);
#if WINDOWS_APP
                    //show progress bar at least 1 sec
                    await Task.WhenAll(saveTask, Task.Delay(1000));
#elif WINDOWS_PHONE_APP
                    await saveTask;
                    var statusBar = StatusBar.GetForCurrentView();
                    statusBar.ProgressIndicator.Text = $"Saved {SelectedPhoto.TitleWithExtension}";
                    await Task.Delay(1000);
#endif
                }
            }
            catch (Exception e)
            {
                e.Log();
                var dialog = new MessageDialog("Failed to save image", "Error");
                await dialog.ShowAsync();
            }
            BusyCounter--;
        });

        public ICommand SharePhoto => new RelayCommand(async () =>
        {
            BusyCounter++;

            try
            {
                mSharedMedia = await SelectedPhoto.GetHighQualityPhotoStreamAsync();

                if (mSharedMedia != null)
                    DataTransferManager.ShowShareUI();
            }
            catch (Exception e)
            {
                e.Log();
                var dialog = new MessageDialog("Failed to share image", "Error");
                await dialog.ShowAsync();
            }

            BusyCounter--;
        });

        public MainPageViewModel()
        {
#if WINDOWS_PHONE_APP
            RegisterForBackButton();
#endif
            RegisterForShare();
        }

#if WINDOWS_PHONE_APP
        private void RegisterForBackButton()
        {
            HardwareButtons.BackPressed += OnBackButtonPressed;
        }

        private void OnBackButtonPressed(object sender, BackPressedEventArgs backPressedEventArgs)
        {
            if (SelectedPhoto == null)
                return;

            SelectedPhoto = null;
            backPressedEventArgs.Handled = true;
        }
#endif

        private void RegisterForShare()
        {
            mDataTransferManager = DataTransferManager.GetForCurrentView();
            mDataTransferManager.DataRequested += OnShareDataRequested;
        }

        private async void OnShareDataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {
            var request = args.Request;
            var defferal = request.GetDeferral();

            request.Data.Properties.Title = SelectedPhoto.PhotoTitle;

            var file = await ApplicationData.Current.TemporaryFolder.CreateFileAsync(SelectedPhoto.TitleWithExtension, CreationCollisionOption.GenerateUniqueName);
            await Helpers.CopyStreamToFileAsync(mSharedMedia, file);

            var storageItems = new List<IStorageItem>();
            storageItems.Add(file);
            request.Data.SetStorageItems(storageItems);

            defferal.Complete();
        }

        private async void UpdatePhotosAsync(LocationRect locationRect)
        {
            BusyCounter++;

            var response = await PanoramioService.GetPhotosAsync(locationRect);

            if (response != null)
            {
                var photosToRemove = Photos.Where(photo => response.Photos.All(responsePhoto => responsePhoto.PhotoId != photo.PhotoId)).ToList();
                foreach (var photo in photosToRemove)
                    Photos.Remove(photo);

                var photosToAdd = response.Photos.Where(responsePhoto => Photos.All(photo => photo.PhotoId != responsePhoto.PhotoId)).ToList();
                foreach (var photo in photosToAdd)
                    Photos.Add(photo);
            }

            BusyCounter--;
        }
    }
}