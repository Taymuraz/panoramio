﻿using System;
using System.Diagnostics;
using Windows.Devices.Geolocation;
#if WINDOWS_APP
using Bing.Maps;
#elif WINDOWS_PHONE_APP
using Panoramio.Data;
using Windows.Foundation;
#endif

namespace Panoramio
{
	public static class Extensions
	{
		public static void Log(this string message)
		{
#if DEBUG
			Debug.WriteLine(message);
#else
			//TODO send error report
#endif
		}

		public static void Log(this Exception e)
		{
			e.Message.Log();
			e.StackTrace.Log();
			e.InnerException?.Log();
		}

#if WINDOWS_APP
		public static Location ToLocation(this BasicGeoposition geoposition)
		{
			return new Location(geoposition.Latitude, geoposition.Longitude);
		}
#elif WINDOWS_PHONE_APP
		public static LocationRect GetBoundingRectangle(this Windows.UI.Xaml.Controls.Maps.MapControl map)
		{
			Geopoint lowerLeft;
			Geopoint upperRight;

			try
			{
				map.GetLocationFromOffset(new Point(0, map.ActualHeight), out lowerLeft);
			}
			catch (ArgumentException)
			{
				lowerLeft = new Geopoint(new BasicGeoposition() { Latitude = -90, Longitude = -180 });
			}
			try
			{
				map.GetLocationFromOffset(new Point(map.ActualWidth, 0), out upperRight);
			}
			catch (ArgumentException)
			{
				upperRight = new Geopoint(new BasicGeoposition() { Latitude = 90, Longitude = 180 });
			}

			return new LocationRect()
			{
				West = lowerLeft.Position.Longitude,
				East = upperRight.Position.Longitude,
				South = lowerLeft.Position.Latitude,
				North = upperRight.Position.Latitude,
			};
		}
#endif
	}
}