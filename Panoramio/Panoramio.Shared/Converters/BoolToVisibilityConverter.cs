﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Panoramio.Converters
{
	class BoolToVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, string language)
		{
			var boolValue = (bool)value;

			var inverse = false;
			var inverseString = parameter as string;
			if (inverseString != null)
				bool.TryParse(inverseString, out inverse);

			boolValue ^= inverse;

			return boolValue ? Visibility.Visible : Visibility.Collapsed;
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language)
		{
			var visibilityValue = (Visibility)value;

			var boolValue = visibilityValue == Visibility.Visible;

			var inverse = false;
			var inverseString = parameter as string;
			if (inverseString != null)
				bool.TryParse(inverseString, out inverse);

			boolValue ^= inverse;

			return boolValue;
		}
	}
}
