﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Panoramio.Converters
{
    public class NullToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var visible = value != null;

            var inverse = false;
            var inverseString = parameter as string;
            if (inverseString != null)
                bool.TryParse(inverseString, out inverse);

            visible ^= inverse;

            return visible ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
