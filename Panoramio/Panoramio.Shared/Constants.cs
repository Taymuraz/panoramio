﻿namespace Panoramio
{
    static class Constants
    {
        public const string PanoramioUri = "http://www.panoramio.com/map/get_panoramas.php?";
        public const string LowQuality = "small";
        public const string HighQuality = "medium";
        public const string PopularPhotoSet = "public";
        public const int PhotoCount = 20;
    }
}