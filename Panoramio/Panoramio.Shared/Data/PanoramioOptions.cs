﻿namespace Panoramio.Data
{
    public class PanoramioOptions
    {
        public string Set { get; set; }
        public int From { get; set; }
        public int To { get; set; }
        public double MinX { get; set; }
        public double MinY { get; set; }
        public double MaxX { get; set; }
        public double MaxY { get; set; }
        public string Size { get; set; }
        public bool MapFilter { get; set; }

        public override string ToString()
        {
            return $"set={Set}&from={From}&to={To}&minx={MinX}&miny={MinY}&maxx={MaxX}&maxy={MaxY}&size={Size}&mapfilter={MapFilter.ToString().ToLowerInvariant()}";
        }
    }
}