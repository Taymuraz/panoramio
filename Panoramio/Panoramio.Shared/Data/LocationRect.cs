﻿namespace Panoramio.Data
{
#if WINDOWS_PHONE_APP
    public class LocationRect
    {
        public double West { get; set; }
        public double East { get; set; }
        public double South { get; set; }
        public double North { get; set; }
    }
#endif
}
