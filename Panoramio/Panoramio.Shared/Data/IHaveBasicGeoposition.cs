﻿using Windows.Devices.Geolocation;

namespace Panoramio.Data
{
    interface IHaveBasicGeoposition
    {
        BasicGeoposition BasicGeoposition { get; }
    }
}