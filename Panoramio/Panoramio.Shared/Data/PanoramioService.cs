﻿using System.Threading.Tasks;
#if WINDOWS_APP
using Bing.Maps;
#endif

namespace Panoramio.Data
{
    public static class PanoramioService
    {
        public static async Task<PanoramioResponse> GetPhotosAsync(LocationRect locationRect)
        {
            var options = new PanoramioOptions()
            {
                From = 0,
                To = Constants.PhotoCount,
                MapFilter = true,
                MinX = locationRect.West,
                MaxX = locationRect.East,
                MinY = locationRect.South,
                MaxY = locationRect.North,
                Set = Constants.PopularPhotoSet,
                Size = Constants.LowQuality,
            };

            var request = PanoramioRequest.Create(options);
            var response = await request.GetResponseAsync();

            return response;
        }
    }
}
