﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;
using Panoramio.ViewModel;

namespace Panoramio.Data
{
    [DataContract]
    public class PanoramioPhoto : ViewModelBase, IHaveBasicGeoposition
    {
        private BitmapImage mLowQualityImage;
        private BitmapImage mHighQualityImage;

        [DataMember(Name = "width")]
        public int Width { get; set; }
        [DataMember(Name = "height")]
        public int Height { get; set; }

        [DataMember(Name = "latitude")]
        public double Latitude { get; set; }
        [DataMember(Name = "longitude")]
        public double Longitude { get; set; }

        [DataMember(Name = "owner_id")]
        public int OwnerId { get; set; }
        [DataMember(Name = "owner_name")]
        public string OwnerName { get; set; }
        [DataMember(Name = "owner_url")]
        public string OwnerUrl { get; set; }

        [DataMember(Name = "photo_file_url")]
        public string PhotoFileUrl { get; set; }
        [DataMember(Name = "photo_id")]
        public int PhotoId { get; set; }
        [DataMember(Name = "photo_title")]
        public string PhotoTitle { get; set; }
        [DataMember(Name = "photo_url")]
        public string PhotoUrl { get; set; }

        [DataMember(Name = "upload_date")]
        public string UploadDate { get; set; }

        public BitmapImage LowQualityImage => mLowQualityImage ?? (mLowQualityImage = new BitmapImage(new Uri(PhotoFileUrl)));
        public BitmapImage HighQualityImage => mHighQualityImage ?? (mHighQualityImage = new BitmapImage(new Uri(HighQualityPhotoUrl)));
        
        public string TitleWithExtension
        {
            get
            {
                var restrictedChars = Path.GetInvalidFileNameChars();
                var fileName = $"{PhotoTitle}.jpg";
                return restrictedChars.Aggregate(fileName, (current, c) => current.Replace(c.ToString(), ""));
            }
        }

        private string HighQualityPhotoUrl => PhotoFileUrl.Replace($"/{Constants.LowQuality}/", $"/{Constants.HighQuality}/");

        public BasicGeoposition BasicGeoposition => new BasicGeoposition() { Latitude = Latitude, Longitude = Longitude };
        
        public async Task<IRandomAccessStream> GetHighQualityPhotoStreamAsync()
        {
            var streamReference = RandomAccessStreamReference.CreateFromUri(new Uri(HighQualityPhotoUrl));
            return await streamReference.OpenReadAsync();
        }
    }
}