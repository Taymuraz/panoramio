﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Panoramio.Data
{
    [DataContract]
    public class PanoramioResponse
    {
        [DataMember(Name = "count")]
        public int Count { get; set; }
        [DataMember(Name = "has_more")]
        public bool HasMore { get; set; }
        [DataMember(Name = "photos")]
        public List<PanoramioPhoto> Photos { get; set; }
        
        public static PanoramioResponse Create(string responseString)
        {
            return SimpleJson.DeserializeObject<PanoramioResponse>(responseString);
        }
    }
}
