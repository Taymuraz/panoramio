﻿using System.Threading.Tasks;

namespace Panoramio.Data
{
    class PanoramioRequest
    {
        private readonly PanoramioOptions mOptions;

        private PanoramioRequest(PanoramioOptions options)
        {
            mOptions = options;
        }

        public static PanoramioRequest Create(PanoramioOptions options)
        {
            return new PanoramioRequest(options);
        }

        public async Task<PanoramioResponse> GetResponseAsync()
        {
            var fullUri = $"{Constants.PanoramioUri}{mOptions}";
            var responseString = await Helpers.GetResponseStringAsync(fullUri);
            return PanoramioResponse.Create(responseString);
        }
    }
}
